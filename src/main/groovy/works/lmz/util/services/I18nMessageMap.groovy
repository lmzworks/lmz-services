package works.lmz.util.services

/**

 * author: Richard Vowles - http://gplus.to/RichardVowles
 */
public interface I18nMessageMap {
  public Map<String, String> getMessagesMap()
  public String getMessagesMapAsJson()
}