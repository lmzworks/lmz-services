package works.lmz.util.services

/**

 * author: Richard Vowles - http://gplus.to/RichardVowles
 */
public interface AngularTemplates {
  public Map<String, String> getAngularTemplates()
  public String getAngularTemplatesAsJson()
}