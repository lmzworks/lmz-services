package works.lmz.util.services

import works.lmz.common.stereotypes.SingletonBean

/**
 * author: Richard Vowles - http://gplus.to/RichardVowles
 */
@SingletonBean
class I18nClientMessageMap extends I18nMessageMapService {
  @Override
  protected String getResourceMatchingPattern() {
    return "client_"
  }
}
