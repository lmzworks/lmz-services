package works.lmz.util.services

import works.lmz.common.jsresource.ApplicationResource
import works.lmz.common.jsresource.ResourceScope
import works.lmz.common.stereotypes.SingletonBean

import javax.inject.Inject

/**
 * Author: Marnix
 *
 * Application resource that exposes angular templates
 */
@SingletonBean
class AngularTemplateResource implements ApplicationResource {

    /**
     * Template service injected here
     */
    @Inject AngularTemplates angularTemplates;

    /**
     * @return the global resource scope
     */
    @Override
    ResourceScope getResourceScope() {
        return ResourceScope.Global;
    }

    /**
     * @return a map of angular templates
     */
    @Override
    Map<String, Object> getResourceMap() {
        return ['templates' : angularTemplates.angularTemplates ]
    }
}
