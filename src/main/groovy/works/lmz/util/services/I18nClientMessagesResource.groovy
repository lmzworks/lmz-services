package works.lmz.util.services

import works.lmz.common.jsresource.ApplicationResource
import works.lmz.common.jsresource.ResourceScope
import works.lmz.common.stereotypes.SingletonBean

import javax.inject.Inject

/**
 * Author: Marnix
 *
 * The internationalization messages map
 */
@SingletonBean
class I18nClientMessagesResource implements ApplicationResource {

    /**
     * Messages map bound here
     */
    @Inject private I18nClientMessageMap messageMap;

    /**
     * @return the global resource scope
     */
    @Override
    ResourceScope getResourceScope() {
        return ResourceScope.Global
    }

    /**
     * @return a map with all internationalization data in it
     */
    @Override
    Map<String, Object> getResourceMap() {
        return [
            i18n : messageMap.messagesMap
        ]
    }
}
